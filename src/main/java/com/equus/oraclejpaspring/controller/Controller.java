package com.equus.oraclejpaspring.controller;

import com.equus.oraclejpaspring.models.Address;
import com.equus.oraclejpaspring.models.Customer;
import com.equus.oraclejpaspring.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/consumer")
public class Controller {
    @Autowired
    private CustomerRepository repository;

    @GetMapping("/insert")
    public void insertConsumer(){
        Customer s = new Customer();
        s.setName("nassim");
        s.setEmail("a@a.com");
        s.setDate(new Date());
        Address a = new Address();
        a.setHouseNumber("1745");
        a.setCity("Aubervilliers");
        s.setAddress(a);
        repository.save(s);
    }

    @GetMapping("/select")
    public List<Customer> showConsumer(){
        Iterable<Customer> iterator = repository.findAll();
        return StreamSupport.stream(iterator.spliterator(), false)
                                           .collect(Collectors.toList());
    }
}
