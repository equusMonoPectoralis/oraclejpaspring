package com.equus.oraclejpaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OraclejpaspringApplication {

    public static void main(String[] args) {
        SpringApplication.run(OraclejpaspringApplication.class, args);
    }

}