package com.equus.oraclejpaspring.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CUSTOMER_SEQ")
    @SequenceGenerator(sequenceName = "CUSTOMER_SEQ", allocationSize = 1, name = "CUSTOMER_SEQ")
    Long id;
    
    String name;
    
    String email;

    @Column(name = "CREATED_DATE")
    Date date;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ADDRESS_ID")
    Address address;

}
