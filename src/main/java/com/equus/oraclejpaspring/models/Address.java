package com.equus.oraclejpaspring.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Address {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ADDRESS_SEQ")
	@SequenceGenerator(sequenceName = "ADDRESS_SEQ", allocationSize = 1, name = "ADDRESS_SEQ")
	private int id;

	@Column(name = "HOUSE_NUMBER")
	private String houseNumber;
	
	@Column(name = "CITY")
	private String city;

	
}